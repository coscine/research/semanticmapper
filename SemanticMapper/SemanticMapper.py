from rdflib.graph import Graph
from rdflib import URIRef

import logging

log = logging.getLogger(__name__)


class SemanticMapper:
    """
    The class responsible for the semantic mapping.
    """

    def __init__(self, applicationprofiles: "list[Graph]", vocabularies: "list[Graph]"):
        self.__applicationProfiles = applicationprofiles
        self.__vocabularies = vocabularies
        self.__cache = {}

    def getMap(self, keyValues: "dict[str, str]"):
        """
        Creates a mapping based on the given application profiles and vocabularies in the constructor.
        Returns the found mappings for the given key-value pairs.
        """

        log.info("Mapping metadata values")

        entries = []
        for key in keyValues.keys():
            entries.append(
                (
                    key,
                    keyValues[key],
                    self.__formatEntity(key),
                    self.__formatEntity(keyValues[key]),
                )
            )

        return self.__getMappings(entries, {})

    def __getMappings(self, entries: list, mapping={}):
        closestApplicationProfile = self.__findClosestAP(
            self.__applicationProfiles, entries, mapping
        )
        if closestApplicationProfile is not None:
            mapping = self.__createApplicationProfileMapping(
                closestApplicationProfile, entries, mapping
            )

        closestVocabulary = self.__findClosestVocabulary(
            self.__vocabularies, entries, mapping
        )
        if closestVocabulary is not None:
            mapping = self.__createVocabularyMapping(
                closestVocabulary, entries, mapping
            )

        return mapping

    def __findClosestAP(self, graphs: "list[Graph]", entries: list, mapping={}):
        closestGraph = None
        closestGraphFoundCount = -1
        for graph in graphs:
            foundCount = 0
            for entry in entries:
                partPredicate = entry[2]
                if partPredicate in mapping and mapping[partPredicate] != None:
                    continue
                if self.__doesSimilarAPValueExist(graph, entry):
                    foundCount += 1
            if foundCount > closestGraphFoundCount:
                closestGraphFoundCount = foundCount
                closestGraph = graph
        return closestGraph

    def __doesSimilarAPValueExist(self, graph: Graph, entry):
        return self.__getSimilarAPValue(graph, entry) != None

    def __getSimilarAPValue(self, graph: Graph, entry):
        predicate = entry[2]
        obj = entry[3]
        if "class" in predicate:
            for (targetSubject, targetPredicate, targetObject) in self.__cachedQuery(
                graph, (None, URIRef("http://www.w3.org/ns/shacl#targetClass"), None)
            ):
                scanObject = self.__formatEntity(targetObject)
                if obj == scanObject:
                    return (
                        URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                        targetObject,
                    )
                for (labelSubject, labelPredicate, labelObject) in self.__cachedQuery(
                    graph,
                    (
                        targetSubject,
                        URIRef("http://www.w3.org/2000/01/rdf-schema#label"),
                        None,
                    ),
                ):
                    scanObject = self.__formatEntity(labelObject)
                    if obj == scanObject:
                        return (
                            URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                            targetObject,
                        )
        else:
            for (targetSubject, targetPredicate, targetObject) in self.__cachedQuery(
                graph, (None, URIRef("http://www.w3.org/ns/shacl#path"), None)
            ):
                scanObject = self.__formatEntity(targetObject)
                if predicate == scanObject:
                    return targetObject
                for (nameSubject, namePredicate, nameObject) in self.__cachedQuery(
                    graph,
                    (targetSubject, URIRef("https://purl.org/coscine/terms/mapper"), None),
                ):
                    scanObject = self.__formatEntity(nameObject)
                    if predicate == scanObject:
                        return targetObject
                for (nameSubject, namePredicate, nameObject) in self.__cachedQuery(
                    graph,
                    (targetSubject, URIRef("http://www.w3.org/ns/shacl#name"), None),
                ):
                    scanObject = self.__formatEntity(nameObject)
                    if predicate == scanObject:
                        return targetObject
        return None

    def __findClosestVocabulary(self, graphs: "list[Graph]", entries: list, mapping={}):
        closestGraph = None
        closestGraphFoundCount = -1
        for graph in graphs:
            foundCount = 0
            for entry in entries:
                partPredicate = entry[2]
                if partPredicate in mapping and mapping[partPredicate] != None:
                    continue
                if self.__doesSimilarVocabularyValueExist(graph, entry):
                    foundCount += 1
            if foundCount > closestGraphFoundCount:
                closestGraphFoundCount = foundCount
                closestGraph = graph
        return closestGraph

    def __doesSimilarVocabularyValueExist(self, graph: Graph, entry):
        return self.__getSimilarVocabularyValue(graph, entry) != None

    def __getSimilarVocabularyValue(self, graph: Graph, entry):
        predicate = entry[2]
        obj = entry[3]
        if "class" in predicate:
            for (targetSubject, targetPredicate, targetObject) in self.__cachedQuery(
                graph,
                (None, None, URIRef("http://www.w3.org/2000/01/rdf-schema#Class")), # TODO: Add owl:Class as well
            ):
                scanSubject = self.__formatEntity(targetSubject)
                if obj == scanSubject:
                    return (
                        URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                        targetSubject,
                    )
                for (labelSubject, labelPredicate, labelObject) in self.__cachedQuery(
                    graph,
                    (
                        targetSubject,
                        URIRef("http://www.w3.org/2000/01/rdf-schema#label"),
                        None,
                    ),
                ):
                    scanObject = self.__formatEntity(labelObject)
                    if obj == scanObject:
                        return (
                            URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                            targetSubject,
                        )
        else:
            for (targetSubject, targetPredicate, targetObject) in self.__cachedQuery(
                graph,
                (
                    None,
                    None,
                    URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"),
                ),
            ):
                scanSubject = self.__formatEntity(targetSubject)
                if predicate == scanSubject:
                    return targetSubject
                for (nameSubject, namePredicate, nameObject) in self.__cachedQuery(
                    graph,
                    (
                        targetSubject,
                        URIRef("http://www.w3.org/2000/01/rdf-schema#label"),
                        None,
                    ),
                ):
                    scanObject = self.__formatEntity(nameObject)
                    if predicate == scanObject:
                        return targetSubject
        return None

    def __cachedQuery(self, graph: Graph, triple):
        if (
            graph.identifier in self.__cache
            and triple in self.__cache[graph.identifier]
        ):
            return self.__cache[graph.identifier][triple]
        returnList = list(graph.triples(triple))
        if graph.identifier not in self.__cache:
            self.__cache[graph.identifier] = {}
        self.__cache[graph.identifier][triple] = returnList
        return returnList

    def __formatEntity(self, entity: str):
        formattedEntity = entity.lower()
        if "#" in formattedEntity:
            formattedEntity = formattedEntity[formattedEntity.rfind("#") + 1 :]
        else:
            formattedEntity = formattedEntity[formattedEntity.rfind("/") + 1 :]
        return formattedEntity

    def __createApplicationProfileMapping(
        self, applicationProfile: Graph, entries: list, mapping={}
    ):
        for entry in entries:
            partPredicate = entry[2]
            if partPredicate not in mapping or mapping[partPredicate] == None:
                mapping[partPredicate] = self.__getSimilarAPValue(
                    applicationProfile, entry
                )
        return mapping

    def __createVocabularyMapping(self, vocabulary: Graph, entries: list, mapping={}):
        for entry in entries:
            partPredicate = entry[2]
            if partPredicate not in mapping or mapping[partPredicate] == None:
                mapping[partPredicate] = self.__getSimilarVocabularyValue(
                    vocabulary, entry
                )
        return mapping
