# SemanticMapper

A python tool which takes metadata values as input (key => value structure) and based on 0-n application profiles, 0-n vocabularies and 0-n defined mappings (e.g. user defined) finds the best fit to represent the keys and values in an interoperable metadata format.

### Installation

Run:

```bash
pip install -r requirements.txt
```

### Server

For running the SemanticMapper as a server, just execute the `server.py` with Python.

```bash
python server.py
```

Set the environment variable `SEMANTICMAPPERPORT` if you want to specify the port and `SEMANTICMAPPERHOST` if you want to specify the host.

### Build

This repository comes with build artifacts which contains an executable that starts the server.

### Example Usage

Navigate to the application after it runs and check out the Swagger interface!

#### Example Request

After the server has been set up and is running, a post request with this JSON data will yield results:

```
{
    "applicationProfiles": [
        {
            "definition": "@base <https://purl.org/coscine/ap/radar/> .\n\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n@prefix sh: <http://www.w3.org/ns/shacl#> .\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n\n@prefix dcterms: <http://purl.org/dc/terms/> .\n\n@prefix coscineradar: <https://purl.org/coscine/ap/radar#> .\n\n<https://purl.org/coscine/ap/radar/>\n  dcterms:license <http://spdx.org/licenses/MIT> ;\n  dcterms:publisher <https://itc.rwth-aachen.de/> ;\n  dcterms:rights \"Copyright © 2020 IT Center, RWTH Aachen University\" ;\n  dcterms:title \"radar application profile\"@en ;\n\n  a sh:NodeShape ;\n  sh:targetClass <https://purl.org/coscine/ap/radar/> ;\n  sh:closed true ; # no additional properties are allowed\n\n  sh:property [\n    sh:path rdf:type ;\n  ] ;\n\n  sh:property coscineradar:creator ;\n  sh:property coscineradar:title ;\n  sh:property coscineradar:created ;  \n  sh:property coscineradar:subject ;\n  sh:property coscineradar:type ;\n  sh:property coscineradar:rights ;\n  sh:property coscineradar:rightsHolder .\n\ncoscineradar:creator\n  sh:path dcterms:creator ;\n  sh:order 0 ;\n  sh:minCount 1 ;\n  sh:maxCount 1 ;\n  sh:minLength 1 ;\n  sh:datatype xsd:string ;\n  sh:defaultValue \"{ME}\" ;\n  sh:name \"Creator\"@en, \"Ersteller\"@de .\n\ncoscineradar:title\n  sh:path dcterms:title ;\n  sh:order 1 ;\n  sh:minCount 1 ;\n  sh:maxCount 1 ;\n  sh:minLength 1 ;\n  sh:datatype xsd:string ;\n  sh:name \"Title\"@en, \"Titel\"@de .\n\ncoscineradar:created\n  sh:path dcterms:created ;\n  sh:order 2 ;\n  sh:minCount 1 ;\n  sh:maxCount 1 ;\n  sh:datatype xsd:date ;\n  sh:defaultValue \"{TODAY}\" ;\n  sh:name \"Production Date\"@en, \"Erstelldatum\"@de .\n\ncoscineradar:subject\n  sh:path dcterms:subject ;\n  sh:order 3 ;\n  sh:maxCount 1 ;\n  sh:name \"Subject Area\"@en, \"Fachrichtung\"@de ;\n  sh:class <http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/> .\n\ncoscineradar:type\n  sh:path dcterms:type ;\n  sh:order 4 ;\n  sh:maxCount 1 ;\n  sh:name \"Resource\"@en, \"Ressource\"@de ;\n  sh:class <http://purl.org/dc/dcmitype/> .\n\ncoscineradar:rights\n  sh:path dcterms:rights ;\n  sh:order 5 ;\n  sh:maxCount 1 ;\n  sh:datatype xsd:string ;\n  sh:name \"Rights\"@en, \"Berechtigung\"@de .\n\ncoscineradar:rightsHolder\n  sh:path dcterms:rightsHolder ;\n  sh:order 6 ;\n  sh:maxCount 1 ;\n  sh:datatype xsd:string ;\n  sh:name \"Rightsholder\"@en, \"Rechteinhaber\"@de .\n"
        }
    ],
    "vocabularies": [],
    "entries": [{
        "creator": "Benedikt Heinrichs",
        "version": "2",
        "storedAt": "file:///text.txt"
    }]
}
```

The results are:

```
[{
  "creator": "http://purl.org/dc/terms/creator",
  "storedat": null,
  "version": null
}]
```

Some example `ApplicationProfiles`, `Examples` and `Vocabularies` can be found in their respective folders.

## Docker

Run the following commands for using this repository with docker (replacing {yourPort}):

```bash
docker build -t semantic-mapper .
docker run --publish {yourPort}:36542 semantic-mapper
```
