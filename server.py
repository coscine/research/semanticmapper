from flask import Flask, request, jsonify
from flask_restx import Api, Resource, fields
import os
import json

from rdflib.graph import Graph

from SemanticMapper.SemanticMapper import SemanticMapper
from SemanticMapper import __version__


app = Flask(__name__)
api = Api(app, version=__version__, title='Semantic Mapper API',
    description='This API maps metadata values to RDF entities',
)


def parseGraphs(key: str, data: dict):
    graphs = []
    if key in data:
        for applicationProfileObject in data[key]:
            rdfFormat = "turtle"
            if "type" in applicationProfileObject:
                rdfFormat = applicationProfileObject["type"]
            g = Graph()
            g.parse(data=str(applicationProfileObject["definition"]), format=rdfFormat)
            graphs.append(g)
    return graphs


definitionObject = api.model('DefinitionObject', { "definition": fields.String, "type": fields.String })

mappingInput = api.model('MappingInput', {
    "applicationProfiles": fields.List(fields.Nested(definitionObject)),
    "vocabularies": fields.List(fields.Nested(definitionObject)),
    "entries": fields.List(fields.Wildcard(fields.String)),
})

@api.route("/")
class SemanticMapperWorker(Resource):
    '''Performs the Semantic Mapping'''
    @api.expect(mappingInput)
    @api.response(200, 'Success', [fields.Wildcard(fields.String)])
    def post(self):
        data = request.json
        if isinstance(data, str):
            data = json.loads(data)
        applicationProfiles = parseGraphs("applicationProfiles", data)
        vocabularies = parseGraphs("vocabularies", data)
        entries = data["entries"]

        semanticMapper = SemanticMapper(applicationProfiles, vocabularies)

        mappings = []
        for entryObject in entries:
            mappings.append(semanticMapper.getMap(entryObject))

        return jsonify(mappings)


versionModel = api.model('Version', {
    "version": fields.String()
})
@api.route("/version")
class VersionWorker(Resource):
    '''Returns the current version of the Semantic Mapper'''
    @api.response(200, 'Success', versionModel)
    def get(self):
        return jsonify({"version": __version__})


if __name__ == "__main__":
    from waitress import serve
    serve(
        app,
        host=os.environ.get("SEMANTICMAPPERHOST", "0.0.0.0"),
        port=os.environ.get("SEMANTICMAPPERPORT", 36542),
    )
