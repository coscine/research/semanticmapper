import logging
from logging.config import dictConfig

import os, os.path


def setDefaultLogging():
    log_dir = "./logs"

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    dictConfig(
        {
            "version": 1,
            "formatters": {
                "console": {
                    "format": "%(message)s",
                },
                "file": {
                    "format": "%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
                },
            },
            "handlers": {
                "stream": {
                    "class": "logging.StreamHandler",
                    "formatter": "console",
                    "level": logging.INFO,
                },
                "debug_file": {
                    "class": "logging.handlers.TimedRotatingFileHandler",
                    "when": "midnight",
                    "utc": True,
                    "backupCount": 7,
                    "level": logging.DEBUG,
                    "filename": "{}/debug.log".format(log_dir),
                    "formatter": "file",
                    "encoding": "utf-8",
                },
                "info_file": {
                    "class": "logging.handlers.TimedRotatingFileHandler",
                    "when": "midnight",
                    "utc": True,
                    "backupCount": 7,
                    "level": logging.INFO,
                    "filename": "{}/info.log".format(log_dir),
                    "formatter": "file",
                    "encoding": "utf-8",
                },
            },
            "root": {
                "handlers": ["stream", "debug_file", "info_file"],
                "level": logging.DEBUG,
            },
        }
    )
