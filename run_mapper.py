import os
import os.path

import json
from rdflib.graph import Graph

current_dir = os.path.dirname(os.path.realpath(__file__))
examples_folder = current_dir + "\\Examples\\"
applicationprofiles_folder = current_dir + "\\ApplicationProfiles\\"
vocabularies_folder = current_dir + "\\Vocabularies\\"

with open(examples_folder + "radar.json", "r", encoding="utf-8") as example_file:
    example = json.loads(example_file.read())
    
def parseGraph(graph):
    g = Graph()
    g.parse(graph, format="turtle")
    return g

from SemanticMapper.SemanticMapper import SemanticMapper

semanticMapper = SemanticMapper([parseGraph(applicationprofiles_folder + "radar.ttl")], [])

output = semanticMapper.getMap(example)

print(output)

with open(current_dir + "\\out.json", "w", encoding="utf-8") as out_file:
    out_file.write(json.dumps(output, sort_keys=True, indent=4, separators=(',', ': ')))
